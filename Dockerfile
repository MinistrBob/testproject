# See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

# Запускать так (сделать текущей папкой src):
# rem cd /C/RosTourism/Source/Integration/src
# cd C:\RosTourism\Source\rostourism-registry-backend\src
# docker build -f Rostourism.Integration.BFF/Dockerfile -t rostourism-integration-bff:v.0.0.YYYMMDDHHMM . 
# docker login registry.gitlab.com
# docker build -f Rostourism.Integration.BFF/Dockerfile -t registry.gitlab.com/i-teco/rostourism-registry-backend/rostourism.integration.bff:v.0.0.YYYMMDDHHMM .
# docker push registry.gitlab.com/i-teco/rostourism-registry-backend/rostourism.integration.bff:v.0.0.YYYMMDDHHMM


FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443

# Locale
# RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y locales
# RUN locale-gen ru_RU.UTF-8
# ENV LANG ru_RU.UTF-8
# ENV LANGUAGE ru_RU:en
# ENV LC_ALL ru_RU.UTF-8

# App
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build

WORKDIR /src

COPY ["Rostourism.Integration.BFF/Rostourism.Integration.BFF.csproj", "Rostourism.Integration.BFF/"]
COPY ["Rostourism.External.Authentication.NetModel/Rostourism.External.Authentication.NetModel.csproj", "Rostourism.External.Authentication.NetModel/"]
COPY ["TokenNetModel/TokenNetModel.csproj", "TokenNetModel/"]
COPY ["UserNetModel/UserNetModel.csproj", "UserNetModel/"]
COPY ["ServiceClient/ServiceClient.csproj", "ServiceClient/"]
COPY ["Rostourism.Constants/Rostourism.SSO.Constants.csproj", "Rostourism.Constants/"]
COPY ["Infrastructure/Rostourism.DAL/Rostourism.DAL.csproj", "Infrastructure/Rostourism.DAL/"]
COPY ["Rostourism.Common/Rostourism.Common.csproj", "Rostourism.Common/"]
COPY ["Infrastructure/Rostourism.DAL.Kafka/Rostourism.DAL.Kafka.csproj", "Infrastructure/Rostourism.DAL.Kafka/"]
COPY ["Rostourism.Integration.BFF.Common/Rostourism.Integration.BFF.Common.csproj", "Rostourism.Integration.BFF.Common/"]
COPY ["NotificationsServiceClient/NotificationsServiceClient.csproj", "NotificationsServiceClient/"]
COPY ["Rostourism.Cache/Rostourism.Cache.csproj", "Rostourism.Cache/"]
COPY ["Rostourism.Cache.Abstractions/Rostourism.Cache.Abstractions.csproj", "Rostourism.Cache.Abstractions/"]
COPY ["Rostourism.Cache.Redis/Rostourism.Cache.Redis.csproj", "Rostourism.Cache.Redis/"]
COPY ["Rostourism.DAL.Redis/Rostourism.DAL.Redis.csproj", "Rostourism.DAL.Redis/"]
COPY ["Rostourism.Common.Constants/Rostourism.Common.Constants.csproj", "Rostourism.Common.Constants/"]

RUN dotnet restore "Rostourism.Integration.BFF/Rostourism.Integration.BFF.csproj"

#COPY ./ ./
COPY Rostourism.Integration.BFF/ Rostourism.Integration.BFF/
COPY Rostourism.External.Authentication.NetModel/ Rostourism.External.Authentication.NetModel/
COPY TokenNetModel/ TokenNetModel/
COPY UserNetModel/ UserNetModel/
COPY ServiceClient/ ServiceClient/
COPY Rostourism.Constants/ Rostourism.Constants/
COPY Rostourism.Common/ Rostourism.Common/
COPY Infrastructure/Rostourism.DAL/ Infrastructure/Rostourism.DAL/
COPY Infrastructure/Rostourism.DAL.Kafka/ Infrastructure/Rostourism.DAL.Kafka/
COPY Rostourism.Integration.BFF.Common/ Rostourism.Integration.BFF.Common/
COPY NotificationsServiceClient/ NotificationsServiceClient/
COPY Rostourism.Cache/ Rostourism.Cache/
COPY Rostourism.Cache.Abstractions/ Rostourism.Cache.Abstractions/
COPY Rostourism.Cache.Redis/ Rostourism.Cache.Redis/
COPY Rostourism.DAL.Redis/ Rostourism.DAL.Redis/
COPY Rostourism.Common.Constants/ Rostourism.Common.Constants/

RUN dotnet build "Rostourism.Integration.BFF/Rostourism.Integration.BFF.csproj" -c Release -o /app/build
FROM build AS publish
RUN dotnet publish "Rostourism.Integration.BFF/Rostourism.Integration.BFF.csproj" -c Release -o /app/publish
FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish ./
ENTRYPOINT ["dotnet", "Rostourism.Integration.BFF.dll"]

